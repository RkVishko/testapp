import React from "react";

export default class CustomButton extends React.Component{
  render(){
    return(
      <div style={{position: 'absolute', top: this.props.top, left: this.props.left}}>
        <button style={{color: 'white', backgroundColor: '#2f678c', borderWidth: 0, width: 150, height: 25, cursor: 'pointer'}}>KARTE ANZEIGEN</button>
      </div>
    )
  }
}
