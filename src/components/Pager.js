import React from "react";

export default class Pager extends React.Component {
  render() {
    return (
      <div style={{
        backgroundColor: 'white',
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        borderBottom: '1px solid grey',
        borderTop: '1px solid grey',
        marginBottom: 10
      }}>
        <div style={{display: 'flex', alignItems: 'center'}}><i
          style={{fontSize: 30, position: 'relative', left: 15, color: 'grey', cursor: 'pointer', alignItems: 'center'}}
          className="icon ion-md-albums"/>
        </div>
        <p style={{color: 'grey', fontSize: 15, display: 'flex', alignItems: 'center'}}>REF.-NR.</p>
        <h3 style={{color: 'black', fontWeight: '400'}}>YF1919054</h3>
        <i style={{fontSize: 40, color: 'grey', marginTop: 5}} className="icon ion-md-arrow-dropleft"
           onClick={this.previous}/>
        <i style={{fontSize: 40, color: 'grey', marginTop: 5}} className="icon ion-md-arrow-dropright"
           onClick={this.previous}/>

      </div>
    )
  }
}
