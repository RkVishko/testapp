import React from "react";
import awardsWrapper from "../assets/awards"

export default class ItemListForWebView extends React.Component {
  render() {
    return (
      <div className={"maps-container-web"}>
        <p>Auszeichnungen</p>
        <div>
          {awardsWrapper.awards.map((item, index) => {
            return (<img key={index} src={item.path}/>)
          })}
        </div>
      </div>
    )
  }
}
