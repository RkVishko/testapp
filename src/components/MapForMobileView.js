import React from "react";
import '../App.css';
import image2 from "../assets/image2.png"

export default class MapForMobileView extends React.Component {
  render() {
    return (
      <div className={"maps-container-mobile"}>
        <div>
          <p>Standort</p>
          <span style={{color: '#2f678c', fontWeight: 'bold'}}>Musterfirma GmbH</span>
          <span>Völklinger Str. 1, 40219 Düsseldorf, Deutschland</span>
        </div>
        <img src={image2}/>
      </div>
    )
  }
}
