import React from "react";
import '../App.css';
import image2 from "../assets/image2.png"
import CustomButton from "./CustomButton";
export default class MapForWebView extends React.Component {

  render() {
    return(
      <div className={"maps-container"}>
        <div style={{position: 'relative'}}>
          <CustomButton top={15} left={45}/>
          <img src={image2}/>
        </div>
        <div>
          <p>Standort</p>
          <span style={{color: '#2f678c', fontWeight: 'bold'}}>Musterfirma GmbH</span>
          <span>Völklinger Str. 1, 40219 Düsseldorf, Deutschland</span>
        </div>
      </div>
    )
  }
}
