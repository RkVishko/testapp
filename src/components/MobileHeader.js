import React from "react";
import '../App.css';

export default class MobileHeader extends React.Component {
  render() {
    return(
      <div className={"header"} style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
        <i style={{fontSize: 40, position: 'absolute', left: 15, color: 'white', cursor: 'pointer'}} className="icon ion-md-arrow-dropleft"/>
        <div><h1 style={{color: 'white', fontSize: 20}}>Your Logo</h1></div>
        <i style={{fontSize: 25, position: 'absolute', right: 15, color: 'white', cursor: 'pointer'}} className="icon ion-md-share"/>
      </div>
    )
  }
}
