import React from "react";
import image1 from "../assets/image1.jpg"
import awardsWrapper from "../assets/awards"

export default class ItemListForMobileView extends React.Component {

  state = {
    currAward: 0
  }

  previous = () => {
    this.setState((state) => {
      if(state.currAward === 0){
        return {currAward: awardsWrapper.awards.length-1}
      }else{
        return {currAward: state.currAward - 1}
      }
    })
  }

  next = () => {
    this.setState((state) => {
      if(awardsWrapper.awards.length === state.currAward + 1){
        return {currAward: 0}
      }else{
        return {currAward: state.currAward + 1}
      }

    })
  }

  render() {
    return (
      <div className={"item-list-container-mobile"}>
        <div>
          <p>Auszeichnungen</p>
        </div>


        <i style={{fontSize: 40, position: 'absolute', left: 15, paddingTop: '35%'}} className="icon ion-md-arrow-dropleft" onClick={this.previous}/>

        <div className={"item-container"}>
          <img src={awardsWrapper.awards[this.state.currAward].path}/>
        </div>

        <i style={{fontSize: 40, position: 'absolute', right: 15, paddingTop: '35%'}} className="icon ion-md-arrow-dropright" onClick={this.next}/>

      </div>
    )
  }
}