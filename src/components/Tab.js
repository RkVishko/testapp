import React from "react";

export default class Tabs extends React.Component {

  state = {
    activeBtn: 0
  }

  checkColor = (c) => {
    if(this.state.activeBtn === c){
      return "green"
    }else{
      return "black"
    }
  }

  changeBtn = (c) => {
    this.setState((state) => {
      return {activeBtn: c}
    })
  }

  checkBorder = (c) => {
    if(this.state.activeBtn === c){
      return '4px solid green'
    }else{
      return "none"
    }
  }


  render() {
    return(
      <div style={{ display: 'flex', flexDirection: 'row', width: '100%'}}>
        <div onClick={ () => this.changeBtn(0)} style={{alignSelf: 'center', justifyContent: 'center', width: '50%', display: 'flex', cursor: 'pointer', fontWeight: 'bold', color: this.checkColor(0), borderBottom: this.checkBorder(0),  padding: '5% 5%'}}>
          ANZEIGE
        </div>
        <div onClick={ () => this.changeBtn(1)} style={{alignSelf: 'center', justifyContent: 'center', width: '50%', display: 'flex', cursor: 'pointer', fontWeight: 'bold', color: this.checkColor(1), borderBottom: this.checkBorder(1), padding: '5% 5%'}}>
          FIRMENPROFIL
        </div>
      </div>
    )
  }
}
