import awards1 from './award1.png'
import awards2 from './award2.jpg'
import awards3 from './award3.png'

export default {
    awards: [
      {id: 0, path: awards1},
      {id: 1, path: awards2},
      {id: 2, path: awards3},
    ]
}