import React, {Component} from 'react';
import './App.css';
import image1 from "./assets/image1.jpg"
import GoogleMap from 'google-map-react';
import MapForWebView from "./components/MapForWebView";
import MapForMobileView from "./components/MapForMobileView";
import ItemListForMobileView from "./components/ItemListForMobileView";
import ItemListForWebView from "./components/ItemListForWebView";
import MobileHeader from "./components/MobileHeader";
import Tabs from "./components/Tab";
import Pager from "./components/Pager";

class App extends Component {

  resize = () => this.forceUpdate()

  componentDidMount(){
    window.addEventListener('resize', this.resize)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resize)
  }

  render() {
    return (
      <div className="App">

        {(window.innerWidth > 600) ? (null) : (<MobileHeader/>)}
        {(window.innerWidth > 600) ? (null) : (<Tabs/>)}
        {(window.innerWidth > 600) ? (null) : (<Pager/>)}

        <div className={"image-container"}>
          <img src={image1} alt={"Image1"}/>
        </div>

        <div className={"card-container"}>
          <div>
            <p>Einleitung</p>
            <span>
              Lorem ipsum dolor sit amet,
              consectetur adipiscing elit. Curabitur dapibus ac nulla non molestie.
              Nulla ac lorem scelerisque, lacinia urna non, euismod eros. Quisque quam erat,
              dignissim et facilisis nec, fermentum ac lacus. Vestibulum molestie augue eu
              mauris convallis iaculis. Pellentesque vitae vulputate ipsum, eu tempus purus.
              Nunc eu volutpat purus. Maecenas leo nibh, dignissim a pulvinar in, elementum id libero.
              Sed dui nunc, tristique in tincidunt at, semper ac nibh. Aliquam congue vel ante sed lacinia.
              Ut in leo ex.
            </span>
          </div>
        </div>

        <div className={"card-container"}>
          <div>
            <p>Ihre Aufgaben</p>
            <ul>
              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
              <li>Curabitur dapibus ac nulla non molestie</li>
              <li>Nulla ac lorem scelerisque</li>
            </ul>
          </div>
        </div>

        <div className={"card-container"}>
          <div>
            <p>Ihr Profil</p>
            <ul>
              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur
                adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit
              </li>
              <li>Curabitur dapibus ac nulla non molestie. urabitur dapibus ac nulla non molestie. urabitur dapibus ac
                nulla non molestie. urabitur dapibus ac nulla non molestie
              </li>
              <li>Nulla ac lorem scelerisque</li>
            </ul>
          </div>
        </div>

        <div className={"card-container"}>
          <div>
            <p>Wir bieten</p>
            <ul>
              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
              <li>Curabitur dapibus ac nulla non molestie. urabitur dapibus ac nulla non molestie</li>
              <li>Nulla ac lorem scelerisque</li>
              <li>urabitur dapibus ac nulla non molestie. urabitur dapibus ac nulla non molestie</li>
            </ul>
          </div>
        </div>

        <div className={"card-container"}>
          <div>
            <p>Kontakt</p>
            <h5>Haben wie Interesse geweckt?</h5>
            <div>
            <span>
              Wir freuen uns auf Ihre aussagekräftige Bewerbung unter Angabe Ihrer Gehaltsvorstellung und des frühestmöglichen Eintrittstermins
              per E-Mail an: <a href={"#"}>job@musterfirma.de.</a>
            </span>
              <div>
                <span>Bei Rückfragen steht Ihnen Herr Max Mustermann gerne unter der <br/> Telefonnummer 0211 / 934 93 5802 zur Verfügung.</span>
              </div>
              <div>
                <span> Musterfirma GmbH</span>
                <span>Musterstraße 24</span>
                <span>12345 Musterstadt</span>
                <a href={"https://www.google.com.ua/"}>www.musterfirma.de</a>
              </div>
            </div>
          </div>
        </div>

        <div className={"video-container"}>
          <iframe
            src="https://www.youtube.com/embed/5Fv19KVVya8">
          </iframe>
        </div>

        {/*<div className={"maps-container"}>*/}
        {/*<GoogleMap*/}
        {/*apiKey={"AIzaSyBEUE7AENYGu8GbNtytt-wXDFk6IdP-TmQ" }*/}
        {/*center={{*/}
        {/*lat: 49.83826,*/}
        {/*lng: 24.02324*/}
        {/*}}*/}
        {/*zoom={11}*/}
        {/*>*/}
        {/*</GoogleMap>*/}
        {/*</div>*/}

        {(window.innerWidth > 600) ? (<MapForWebView/>) : (<MapForMobileView/>)}

        {(window.innerWidth > 600) ? (<ItemListForWebView/>) : (<ItemListForMobileView/>)}




      </div>
    );
  }
}

export default App;
